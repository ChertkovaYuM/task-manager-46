package ru.tsc.chertkova.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.service.ITokenService;

@Getter
@Setter
public class TokenService implements ITokenService {

    @NotNull
    private String token;

}
