package ru.tsc.chertkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.model.User;

import java.util.List;

public interface IUserRepository extends IAbstractRepository<User> {

    void add(@NotNull User model);

    void clear();

    @NotNull
    List<User> findAll();

    @Nullable
    User findById(@NotNull String id);

    int getSize();

    void removeById(@NotNull String id);

    void update(User model);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

    long isLoginExist(@NotNull String login);

    long isEmailExist(@NotNull String email);

    void changeRole(@NotNull String id,
                    @NotNull Role role);

    int existsById(@NotNull String id);

    void setPassword(@NotNull String id,
                     @NotNull String passwordHash);

    void setLockedFlag(@NotNull String login,
                       @NotNull Boolean locked);

}
