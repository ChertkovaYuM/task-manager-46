package ru.tsc.chertkova.tm.repository;

import org.junit.experimental.categories.Category;
import ru.tsc.chertkova.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

//    @NotNull
//    private final IPropertyService propertyService = new PropertyService();
//
//    @NotNull
//    private final IConnectionService connectionService = new ConnectionService(propertyService);
//
//    @NotNull
//    private final IProjectRepository repository = new ProjectRepository(connectionService.getConnection());
//
//    @Test
//    public void add() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        repository.add(USER1_PROJECT1);
//        Assert.assertEquals(USER1_PROJECT1, repository.findAll().get(0));
//    }
//
//    @Test
//    public void addByUserId() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        repository.add(USER1.getId(), USER1_PROJECT1);
//        Assert.assertEquals(USER1_PROJECT1, repository.findAll().get(0));
//    }
//
//    @Test
//    public void addAll() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        addAll(repository, PROJECT_LIST);
//        Assert.assertEquals(PROJECT_LIST, repository.findAll());
//    }
//
//    private void addAll(IProjectRepository repository, List<Project> projects) {
//        for (Project p :
//                projects) {
//            repository.add(p);
//        }
//    }
//
//    @Test
//    public void remove() {
//        @NotNull final List<Project> list = new ArrayList<>(USER1_PROJECT_LIST);
//        addAll(repository, USER1_PROJECT_LIST);
//        //@Nullable final Project removed = repository.remove(USER1_PROJECT2);
//        //Assert.assertEquals(USER1_PROJECT2, removed);
//        list.remove(USER1_PROJECT2);
//        Assert.assertEquals(list, repository.findAll());
//        //Assert.assertNull(repository.remove(USER1_PROJECT2));
//    }
//
//    @Test
//    public void removeByUserId() {
//        @NotNull final List<Project> list = new ArrayList<>(PROJECT_LIST);
//        addAll(repository, PROJECT_LIST);
//        repository.remove(USER1.getId(), USER1_PROJECT2);
//        list.remove(USER1_PROJECT2);
//        Assert.assertEquals(list, repository.findAll());
//        repository.remove(USER1.getId(), ADMIN1_PROJECT1);
//        Assert.assertTrue(PROJECT_LIST.contains(ADMIN1_PROJECT1));
//        list.remove(ADMIN1_PROJECT1);
//        Assert.assertNotEquals(list, repository.findAll());
//    }
//
//    @Test
//    public void clear() {
//        addAll(repository, PROJECT_LIST);
//        repository.clear();
//        Assert.assertTrue(repository.findAll().isEmpty());
//    }
//
//    @Test
//    public void clearByUserId() {
//        addAll(repository, PROJECT_LIST);
//        repository.clear(USER1.getId());
//        Assert.assertFalse(repository.findAll().containsAll(USER1_PROJECT_LIST));
//        @NotNull final List<Project> result = new ArrayList<>(PROJECT_LIST);
//        result.removeAll(USER1_PROJECT_LIST);
//        Assert.assertEquals(repository.findAll(), result);
//    }
//
//    @Test
//    public void findAll() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        addAll(repository, PROJECT_LIST);
//        Assert.assertEquals(PROJECT_LIST, repository.findAll());
//    }
//
//    @Test
//    public void findAllByUserId() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        addAll(repository, PROJECT_LIST);
//        Assert.assertEquals(ADMIN1_PROJECT_LIST, repository.findAll(ADMIN1.getId()));
//    }
//
//    @Test
//    public void findById() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        addAll(repository, PROJECT_LIST);
//        Assert.assertEquals(
//                repository.findById(USER1.getId(), USER1_PROJECT2.getId()),
//                USER1_PROJECT2
//        );
//        Assert.assertNull(repository.findById(USER1.getId(), ADMIN1_PROJECT2.getId()));
//    }
//
//    @Test
//    public void removeById() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        addAll(repository, PROJECT_LIST);
//        Assert.assertEquals(
//                repository.removeById(ADMIN1.getId(), ADMIN1_PROJECT2.getId()),
//                ADMIN1_PROJECT2
//        );
//        @NotNull final List<Project> list = new ArrayList<>(PROJECT_LIST);
//        list.remove(ADMIN1_PROJECT2);
//        Assert.assertEquals(repository.findAll(), list);
//        Assert.assertNull(repository.removeById(USER1.getId(), ADMIN1_PROJECT2.getId()));
//        Assert.assertEquals(repository.findAll(), list);
//    }

}
